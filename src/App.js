import { useState } from "react";

import "./App.css";
import "@mantine/core/styles.css";
import * as PropTypes from "prop-types";
import {
  AppShell,
  Burger,
  Button,
  createTheme,
  Divider,
  Group,
  MantineProvider,
  Modal,
  Select,
  Slider,
} from "@mantine/core";

import { IconFlask2 } from "@tabler/icons-react";

import Plots from "./plots";

AppShell.propTypes = {
  padding: PropTypes.string,
  navbar: PropTypes.shape({
    collapsed: PropTypes.shape({ mobile: PropTypes.bool }),
    width: PropTypes.shape({
      md: PropTypes.number,
      lg: PropTypes.number,
      base: PropTypes.number,
    }),
    breakpoint: PropTypes.string,
  }),
  header: PropTypes.shape({
    height: PropTypes.shape({
      md: PropTypes.number,
      lg: PropTypes.number,
      base: PropTypes.number,
    }),
  }),
  children: PropTypes.node,
};

const theme = createTheme({
  primaryColor: "dark",
});

function App() {
  const [opened, setOpened] = useState(true);
  const toggle = () => setOpened(!opened);

  const [P, setP] = useState(1); // bar
  const [L12, setL12] = useState(1); // -
  const [L21, setL21] = useState(1); // -
  const [x1, setX1] = useState(0.5); // -
  const [compound1, setCompound1] = useState("Ethanol");
  const [compound2, setCompound2] = useState("Toluene");

  const [modalOpen, setModalOpen] = useState(false);

  const compounds = [
    "Acetone",
    "Acetonitrile",
    "Benzene",
    "1-Butanol",
    "Cyclohexane",
    "Cyclohexene",
    "Diethyl Ether",
    "Ethanol",
    "Ethyl Acetate",
    "n-Heptane",
    "n-Hexane",
    "Isopropanol",
    "Methanol",
    "Methylcyclohexane",
    "1-Propanol",
    "Tetrahydrofuran",
    "Toluene",
  ];

  return (
    <div className="App">
      <MantineProvider theme={theme}>
        <AppShell
          header={{ height: { base: 60, md: 70, lg: 80 } }}
          navbar={{
            width: { base: 200, md: 300, lg: 400 },
            breakpoint: "sm",
            collapsed: { mobile: !opened },
          }}
          padding="md"
        >
          <AppShell.Header>
            <Group h="100%" px="md">
              <Burger
                opened={opened}
                onClick={toggle}
                hiddenFrom="sm"
                size="sm"
              />
              <img src="/logo.svg" alt="logo" style={{ width: "170px" }} />
            </Group>
          </AppShell.Header>
          <AppShell.Navbar p="md" style={{ textAlign: "left" }}>
            {/* Sophie Code here */}

            <span>
              {" "}
              <i>P</i> / bar{" "}
            </span>
            <Slider
              value={P}
              onChange={setP}
              min={0.5}
              max={5}
              marks={[
                { value: 0.5, label: "0.5" },
                { value: 1, label: "1" },
                { value: 5, label: "5" },
              ]}
              step={0.001}
              mb="xl"
            />

            <span>
              {" "}
              <i>
                Λ<sub>12</sub>{" "}
              </i>
            </span>
            <Slider
              value={L12}
              onChange={setL12}
              min={0.1}
              max={5}
              marks={[
                { value: 0.1, label: "0.1" },
                { value: 1, label: "1" },
                { value: 5, label: "5" },
              ]}
              step={0.001}
              mb="xl"
            />

            <span>
              {" "}
              <i>Λ</i>
              <sub>21</sub>
            </span>
            <Slider
              value={L21}
              onChange={setL21}
              min={0.1}
              max={5}
              marks={[
                { value: 0.1, label: "0.1" },
                { value: 1, label: "1" },
                { value: 5, label: "5" },
              ]}
              step={0.001}
              mb="xl"
            />

            <span>
              {" "}
              <i>x</i>
              <sub>1</sub>
            </span>
            <Slider
              value={x1}
              onChange={setX1}
              min={0}
              max={1}
              marks={[
                { value: 0, label: "0" },
                { value: 0.5, label: "0.5" },
                { value: 1, label: "1" },
              ]}
              step={0.001}
              mb="xl"
            />

            <Select
              data={compounds}
              value={compound1}
              onChange={setCompound1}
              allowDeselect={false}
              mt="md"
              label="Compound 1"
            />

            <Select
              data={compounds}
              value={compound2}
              onChange={setCompound2}
              allowDeselect={false}
              mt="md"
              label="Compound 2"
            />

            {/* Sophie Code end */}

            <Divider
              my="xl"
              labelPosition="center"
              label={<IconFlask2 stroke={2} />}
            />

            <Button onClick={() => setModalOpen(true)}>
              R Functions for Evaluation
            </Button>
          </AppShell.Navbar>
          <AppShell.Main>
            <Plots
              compound1={compound1}
              compound2={compound2}
              P={P}
              x1={x1}
              L12={L12}
              L21={L21}
            />
          </AppShell.Main>
        </AppShell>
        <Modal
          opened={modalOpen}
          onClose={() => setModalOpen(false)}
          title="R functions for evaluation"
          size="xl"
        >
          <pre style={{ overflowX: "auto", padding: "8px" }}>
            <code>
              antoine {"<"}- function(T, c) {"{"} # temperature T in °C, array
              of NIST Antoine parameters
              <br />
              {"  "}return(10^(c[1] - c[2] / (T + 273.15 + c[3])))
              <br />
              {"}"}
              <br />
              <br />
              wilson1 {"<"}- function(x1, L12, L21) {"{"}
              <br />
              {"  "}x2 {"<"}- 1 - x1
              <br />
              {"  "}lng {"<"}- -log(x1 + L12 * x2) + x2*(L12 / (x1 + L12 * x2) -
              L21 / (L21 * x1 + x2))
              <br />
              {"  "}return(exp(lng))
              <br />
              {"}"}
              <br />
              <br />
              wilson2 {"<"}- function(x2, L12, L21) {"{"}
              <br />
              {"  "}x1 {"<"}- 1 - x2
              <br />
              {"  "}lng {"<"}- -log(x2 + L12 * x1) - x1*(L12 / (x1 + L12 * x2) -
              L21 / (x2 + L21 * x1))
              <br />
              {"  "}return(exp(lng))
              <br />
              {"}"}
              <br />
              <br />
              raoult1 {"<"}- function(x1, PV, L12, L21) {"{"} # liquid molar
              fraction x1, vapour pressure PV, lam
              <br />
              {"  "}return(x1 * wilson1(x1, L12, L21) * PV / P)
              <br />
              {"}"}
              <br />
              <br />
              raoult2 {"<"}- function(x2, PV, L12, L21) {"{"} # liquid molar
              fraction x2, vapour pressure PV, lam
              <br />
              {"  "}return(x2 * wilson2(x2, L21, L12) * PV / P)
              <br />
              {"}"}
              <br />
              <br />
              rootfinding {"<"}- function(T, x1, L12, L21) {"{"} # temperature T
              in °C, liquid molar fraction x1,
              <br />
              {"  "}return(
              <br />
              {"  "}
              {"  "}raoult1(x1, antoine(T, antoine_Ac), L12, L21)
              <br />
              {"  "}
              {"  "}+ raoult2(1 - x1, antoine(T, antoine_Cy), L12, L21)
              <br />
              {"  "}
              {"  "}- 1<br />
              {"  "})<br />
              {"}"}
              <br />
            </code>
          </pre>
        </Modal>
      </MantineProvider>
    </div>
  );
}

export default App;
