import { useState, useEffect } from "react";

import { get_T, get_xg } from "./functions";
import Plot from "react-plotly.js";
import { Grid } from "@mantine/core";
import { linear } from "everpolate";

export default function Plots({ compound1, compound2, P, x1, L12, L21 }) {
  var linear = require("everpolate").linear;

  const [xl, setXl] = useState([]);
  const [xg, setXg] = useState([]);
  const [xgi, setXgi] = useState([]);
  const [T, setT] = useState([]);
  const [Ti, setTi] = useState([]);
  const [Pg, setPg] = useState([]);
  const [Pgi, setPgi] = useState([]);

  const [DSTx, setDSTx] = useState([]);
  const [DSTxl, setDSTxl] = useState([]);
  const [DSTy, setDSTy] = useState([]);
  const [DSTT, setDSTT] = useState([]);

  const evaluate = (N) => {
    let xltmp = [];
    let Ttmp = [];
    let Titmp = [];
    let xgtmp = [];
    let xgitmp = [];
    let Pgtmp = [];
    let Pgitmp = [];

    let step = 1 / (N - 1);

    for (let i = 0; i < N; i++) {
      xltmp.push(step * i);
      Ttmp.push(get_T(xltmp[i], L12, L21, compound1, compound2, P));
      Titmp.push(get_T(xltmp[i], 1, 1, compound1, compound2, P));
      xgtmp.push(get_xg(xltmp[i], L12, L21, Ttmp[i], compound1, P));
      xgitmp.push(get_xg(xltmp[i], 1, 1, Titmp[i], compound1, P));
      Pgtmp.push(xgtmp[i] * P);
      Pgitmp.push(xgitmp[i] * P);
    }
    setXl(xltmp);
    setT(Ttmp);
    setTi(Titmp);
    setXg(xgtmp);
    setXgi(xgitmp);
    setPg(Pgtmp);
    setPgi(Pgitmp);
  };

  const evaluateDST = (N) => {
    if (xl.length < 1 || xg.length < 1) return;
    let Dx = [0];
    let Dy = linear([x1], xl, xg);
    let Dl = [x1];

    const dn = 1 / N;
    let ng = 0;
    let n1 = x1;
    let n2 = 1 - x1;
    let x = x1;

    for (let i = 0; i < N; i++) {
      ng += dn;
      const dn1 = dn * Dy[i];
      const dn2 = dn - dn1;
      n1 -= dn1;
      n2 -= dn2;
      x = n1 / (n1 + n2);
      Dx.push(ng);
      Dy.push(linear(x, xl, xg)[0]);
      Dl.push(x);
    }

    const DT = linear(Dy, xl, T);

    setDSTx(Dx.slice(0, -1));
    setDSTy(Dy.slice(0, -1));
    setDSTxl(Dl.slice(0, -1));
    setDSTT(DT.slice(0, -1));
  };

  useEffect(() => {
    if (!compound1) return;
    if (!compound2) return;
    evaluate(101);
  }, [compound1, compound2, P, x1, L12, L21]);

  useEffect(() => {
    evaluateDST(101);
  }, [xl, xg, x1]);

  return (
    <div>
      <Grid>
        <Grid.Col span={{ xs: 12, md: 12 }}>
          <Plot
            data={[
              {
                x: xl,
                y: T,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                name: "real",
              },
              {
                x: xl,
                y: Ti,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dash" },
                name: "ideal",
              },
              {
                x: xg,
                y: T,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                showlegend: false,
                name: "real",
              },
              {
                x: xgi,
                y: Ti,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dash" },
                showlegend: false,
                name: "ideal",
              },
            ]}
            layout={{
              width: "100%",
              height: "100%",
              xaxis: {
                title: "<i>x</i><sub>1</sub><sup>(l)/(g)</sup>",
              },
              yaxis: {
                title: "<i>θ</i> / °C",
              },
            }}
          />
        </Grid.Col>

        <Grid.Col span={{ xs: 12, md: 6 }}>
          <Plot
            data={[
              {
                x: xl,
                y: xg,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                name: "real",
              },
              {
                x: xl,
                y: xgi,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dash" },
                name: "ideal",
              },
              {
                x: [0, 1],
                y: [0, 1],
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dot" },
                name: "<i>x</i><sub>1</sub><sup>(l)</sup> = <i>x</i><sub>1</sub><sup>(g)</sup>",
              },
            ]}
            layout={{
              width: "100%",
              height: "100%",
              xaxis: {
                title: "<i>x</i><sub>1</sub><sup>(l)</sup>",
              },
              yaxis: {
                title: "<i>x</i><sub>1</sub><sup>(g)</sup>",
              },
            }}
          />
        </Grid.Col>

        <Grid.Col span={{ xs: 12, md: 6 }}>
          <Plot
            data={[
              {
                x: xl,
                y: Pg,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                name: "real",
              },
              {
                x: xl,
                y: Pgi,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dash" },
                name: "ideal",
              },
              {
                x: xl,
                y: Pg.map((val) => P - val),
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                showlegend: false,
                name: "real",
              },
              {
                x: xl,
                y: Pgi.map((val) => P - val),
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dash" },
                showlegend: false,
                name: "ideal",
              },
              {
                x: [0, 1],
                y: [P, P],
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                showlegend: false,
              },
            ]}
            layout={{
              width: "100%",
              height: "100%",
              xaxis: {
                title: "<i>x</i><sub>1</sub><sup>(l)</sup>",
              },
              yaxis: {
                title: "<i>P</i><sub>1</sub> / mbar",
              },
            }}
          />
        </Grid.Col>

        <Grid.Col span={{ xs: 12, md: 6 }}>
          <Plot
            data={[
              {
                x: DSTx,
                y: DSTy,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                name: "distillate",
              },
              {
                x: DSTx,
                y: DSTxl,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
                line: { dash: "dash" },
                name: "bottom",
              },
            ]}
            layout={{
              width: "100%",
              height: "100%",
              xaxis: {
                title:
                  "<i>n</i><sub>tot</sub><sup>(g)</sup> / <i>n</i><sub>tot</sub>",
              },
              yaxis: {
                title: "<i>x</i><sub>1</sub><sup>(l)/(g)</sup>",
              },
            }}
          />
        </Grid.Col>

        <Grid.Col span={{ xs: 12, md: 6 }}>
          <Plot
            data={[
              {
                x: DSTx,
                y: DSTT,
                type: "scatter",
                mode: "lines",
                marker: { color: "black" },
              },
            ]}
            layout={{
              width: "100%",
              height: "100%",
              xaxis: {
                title:
                  "<i>n</i><sub>tot</sub><sup>(g)</sup> / <i>n</i><sub>tot</sub>",
              },
              yaxis: {
                title: "<i>θ</i> / °C",
              },
            }}
          />
        </Grid.Col>
      </Grid>
    </div>
  );
}
